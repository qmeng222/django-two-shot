from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

# Register your models with the admin so that you can see them in the site:

admin.site.register(ExpenseCategory)
admin.site.register(Receipt)
admin.site.register(Account)
